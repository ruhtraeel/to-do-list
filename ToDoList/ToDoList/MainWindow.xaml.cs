﻿using Microsoft.Kinect;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;
using Microsoft.Kinect.Toolkit;
using Microsoft.Kinect.Toolkit.Controls;
using System.Collections;
using System.Windows.Media.Animation;
using System.Windows.Threading;
using System.Globalization;
using LightBuzz.Vitruvius;
using System.Speech.Recognition;
using System.Windows.Controls.Primitives;
using System.Text.RegularExpressions;


namespace ToDoList
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {


        private KinectSensorChooser sensorChooser;

        KinectSensor _kinect = null;


        // Gesture Detectors
        GestureController gestureController;

        // Speech recognition
        public SpeechRecognitionEngine speechEngine = new SpeechRecognitionEngine();
        
        
        DispatcherTimer timer = new DispatcherTimer();


        List<Task> tasks = new List<Task>();

        Skeleton [] skeletonData;
        int oldBodyCount = 0;
        double scrollAmount = 0;
        bool scrollForward = true;
        DateTime selectedPeriod = DateTime.Now;
        string currentSelection = "";

        private byte[] _pixelData = new byte[0];

        public static int timeLimit = 2000;

        public MainWindow()
        {
            InitializeComponent();
            InitializeKinect();
            Loaded += OnLoaded;
        }

        private void InitializeKinect()
        {
            // Get  Kinect Sensor
            _kinect = KinectSensor.KinectSensors.FirstOrDefault(sensor => sensor.Status == KinectStatus.Connected);
            
            if (_kinect == null) return;

            // Start Kinect sensor
            //_kinect.Start();
            timer.Interval = new TimeSpan(0, 0, 0, 0, 10);
            timer.Tick += timer_Tick;
            currentPeriod.Text = selectedPeriod.ToString("MMMM") + ", " + selectedPeriod.Year + " tasks";
            dueDate.DisplayDateStart = DateTime.Today;
            mainWindow.Background = new ImageBrush(new BitmapImage(new Uri(BaseUriHelper.GetBaseUri(this), "Images/Background.jpg")));
            gestureController = new GestureController(GestureType.All);
            gestureController.GestureRecognized += GestureController_GestureRecognized;

            speechEngine.LoadGrammar(new DictationGrammar());
            speechEngine.SpeechRecognized += speechEngine_SpeechRecognized;
            speechEngine.SetInputToDefaultAudioDevice(); // set input to default audio device
            speechEngine.RecognizeAsync(RecognizeMode.Multiple); // recognize speech 
        }

        void speechEngine_SpeechRecognized(object sender, SpeechRecognizedEventArgs e)
        {
            if (addTaskGrid.Visibility == Visibility.Visible) 
            {
                if (e.Result.Text == "Microsoft Kinect task") {
                    taskNameBox.Focus();
                    currentSelection = "task";
                    taskNameLabel.Foreground = Brushes.Green;
                    detailsLabel.Foreground = Brushes.White;
                    dueDateLabel.Foreground = Brushes.White;
                }
                else if (e.Result.Text == "Microsoft Kinect details")
                {
                    detailsBox.Focus();
                    currentSelection = "details";
                    detailsLabel.Foreground = Brushes.Green;
                    taskNameLabel.Foreground = Brushes.White;
                    dueDateLabel.Foreground = Brushes.White;
                }
                else if (e.Result.Text == "Microsoft Kinect due dates")
                {
                    currentSelection = "date";
                    dueDateLabel.Foreground = Brushes.Green;
                    taskNameLabel.Foreground = Brushes.White;
                    detailsLabel.Foreground = Brushes.White;
                }
                else if (e.Result.Text == "Microsoft Kinect add")
                {
                    addButton.RaiseEvent(new RoutedEventArgs(ButtonBase.ClickEvent));
                }
                else if (e.Result.Text == "Microsoft Kinect cancel")
                {
                    cancelButton.RaiseEvent(new RoutedEventArgs(ButtonBase.ClickEvent));
                }
                else if (currentSelection == "task")
                {
                    taskNameBox.Text = e.Result.Text;
                }
                else if (currentSelection == "details")
                {
                    detailsBox.Text = e.Result.Text;
                }
                else if (currentSelection == "date" && DateTime.TryParse(e.Result.Text, out selectedPeriod))
                {
                    dueDate.SelectedDate = selectedPeriod;
                }
                
            }
            voiceLabel.Text = "I heard: \"" + e.Result.Text + "\"";
            Console.WriteLine("You said: " + e.Result.Text);
        }



        private void OnLoaded(object sender, RoutedEventArgs routedEventArgs)
        {

            this.sensorChooser = new KinectSensorChooser();
            this.sensorChooser.KinectChanged += SensorChooserOnKinectChanged;
            this.sensorChooserUi.KinectSensorChooser = this.sensorChooser;
            this.sensorChooser.Start();


            _kinect.SkeletonStream.Enable();
            _kinect.SkeletonFrameReady += Sensor_SkeletonFrameReady;
            
            
        }

        private void SensorChooserOnKinectChanged(object sender, KinectChangedEventArgs args)
        {
            bool error = false;
            if (args.OldSensor != null)
            {
                try
                {
                    args.OldSensor.DepthStream.Range = DepthRange.Default;
                    args.OldSensor.SkeletonStream.EnableTrackingInNearRange = false;
                    args.OldSensor.DepthStream.Disable();
                    args.OldSensor.SkeletonStream.Disable();
                }
                catch (InvalidOperationException)
                {
                    // KinectSensor might enter an invalid state while enabling/disabling streams or stream features.
                    // E.g.: sensor might be abruptly unplugged.
                    error = true;
                }
            }

            if (args.NewSensor != null)
            {
                try
                {
                    args.NewSensor.DepthStream.Enable(DepthImageFormat.Resolution640x480Fps30);
                    args.NewSensor.SkeletonStream.Enable();
                    args.NewSensor.SkeletonFrameReady += Sensor_SkeletonFrameReady;

                    try
                    {
                        args.NewSensor.DepthStream.Range = DepthRange.Near;
                        args.NewSensor.SkeletonStream.EnableTrackingInNearRange = true;
                        args.NewSensor.SkeletonStream.TrackingMode = SkeletonTrackingMode.Seated;
                    }
                    catch (InvalidOperationException)
                    {
                        // Non Kinect for Windows devices do not support Near mode, so reset back to default mode.
                        args.NewSensor.DepthStream.Range = DepthRange.Default;
                        args.NewSensor.SkeletonStream.EnableTrackingInNearRange = false;
                        error = true;
                    }

                }
                catch (InvalidOperationException)
                {
                    error = true;
                    // KinectSensor might enter an invalid state while enabling/disabling streams or stream features.
                    // E.g.: sensor might be abruptly unplugged.
                }
            }

            if (!error)
                kinectRegion.KinectSensor = args.NewSensor;

            
        }



        void GestureController_GestureRecognized(object sender, GestureEventArgs e)
        {
            //Debug.WriteLine("Detected");
            //if gest is swipe to right go to the next picture
            if (e.Name == "SwipeRight" && addTaskGrid.Visibility == Visibility.Collapsed)
            {
                //Debug.WriteLine("RightSwipe");
                selectedPeriod = selectedPeriod.AddMonths(-1);
                currentPeriod.Text = selectedPeriod.ToString("MMMM") + ", " + selectedPeriod.Year + " tasks";
                refreshTasks();
            }
            //if gest is swipe to left go to the previous picture
            if (e.Name == "SwipeLeft" && addTaskGrid.Visibility == Visibility.Collapsed)
            {
                //Debug.WriteLine("LeftSwipe");
                selectedPeriod = selectedPeriod.AddMonths(1);
                currentPeriod.Text = selectedPeriod.ToString("MMMM") + ", " + selectedPeriod.Year + " tasks";
                refreshTasks();
            }
            if (e.Name == "SwipeUp" && addTaskGrid.Visibility == Visibility.Collapsed)
            {
                //Debug.WriteLine("TopSwipe");
            }
        }



        private void Sensor_SkeletonFrameReady(object sender, SkeletonFrameReadyEventArgs e)
        {
            using (var frame = e.OpenSkeletonFrame())
            {
                if (frame != null)
                {
                    skeletonData = new Skeleton[frame.SkeletonArrayLength];
                    frame.CopySkeletonDataTo(skeletonData);

                    if (getBodyCount() == 0)
                    {
                        scrollViewerDouble1.Visibility = Visibility.Visible;
                        scrollViewerDouble2.Visibility = Visibility.Visible;
                        scrollContent.Visibility = Visibility.Collapsed;
                        lisaDoubleTaskLabel.Visibility = Visibility.Visible;
                        timDoubleTaskLabel.Visibility = Visibility.Visible;
                        singleTaskLabel.Visibility = Visibility.Collapsed;
                        addTaskButton.Visibility = Visibility.Collapsed;
                        addTaskGrid.Visibility = Visibility.Collapsed;
                    }
                    else
                    {
                        addTaskButton.Visibility = Visibility.Visible;
                        scrollViewerDouble1.Visibility = Visibility.Collapsed;
                        scrollViewerDouble2.Visibility = Visibility.Collapsed;
                        scrollContent.Visibility = Visibility.Visible;
                        lisaDoubleTaskLabel.Visibility = Visibility.Collapsed;
                        timDoubleTaskLabel.Visibility = Visibility.Collapsed;
                        singleTaskLabel.Visibility = Visibility.Visible;
                        
                        

                        if (getBodyCount() == 1)
                            singleTaskLabel.Text = "Lisa's tasks";
                        else if (getBodyCount() == 2)
                            singleTaskLabel.Text = "Lisa & Tim's tasks";
                    }

                    if (getBodyCount() != oldBodyCount)
                    {
                        refreshTasks();
                    }

                    if (addTaskGrid.Visibility == Visibility.Visible)
                    {
                        singleTaskLabel.Visibility = Visibility.Collapsed;
                    }

                    if (skeletonData.Length > 0)
                    {
                        var skeleton = skeletonData[0];
                        foreach (Skeleton selectedBody in skeletonData) {
                            if (selectedBody.TrackingState == SkeletonTrackingState.Tracked)
                                skeleton = selectedBody;
                        }
                        
                        foreach (KinectTileButton button in scrollContent.Children)
                        {
                            if (100 / (skeleton.Position.Z * 0.2) > 100 && 100 / (skeleton.Position.Z * 0.2) < 600)
                            {
                                button.Height = 100 / (skeleton.Position.Z * 0.2);
                                button.Width = 100 / (skeleton.Position.Z * 0.2);
                            }
                        }
                        gestureController.Update(skeleton);
                        //skeletons.Where(
                                   //u => u.TrackingState == SkeletonTrackingState.Tracked).FirstOrDefault();
                        //
                        if (skeleton != null)
                        {
                            
                            //_gesture.Update(user);
                        }
                    }

                    oldBodyCount = getBodyCount();
                }
            }
        }



        public int getBodyCount()
        {
            int bodyCount = 0;
            foreach (Skeleton skeleton in skeletonData)
            {
                if (skeleton.TrackingState == SkeletonTrackingState.Tracked)
                {
                    bodyCount++;
                }
            }
            return bodyCount;
        }

        public void refreshTasks()
        {
                timer.Stop();
                Debug.WriteLine("Bodies: " + getBodyCount());

                // Clear all tasks
                scrollContent.Children.Clear();
                scrollContentTim.Children.Clear();
                scrollContentLisa.Children.Clear();
                foreach (Task task in tasks)
                {
                  
                    var button = new KinectTileButton
                            {
                                Name = task.taskID,
                                FontSize = 20,
                                Content = task.taskName + "\n\n" + "Due: " + task.dueDate.ToShortDateString() + "\n\nDetails: " + task.details,
                                Height = 100,
                                HorizontalLabelAlignment = HorizontalAlignment.Left,
                                Foreground = Brushes.Black,
                                Background = Brushes.Beige
                            };
                    button.Click += removeTask_Click;
                    if (task.dueDate < DateTime.Now) 
                    {
                        button.Background = Brushes.PaleVioletRed;
                    }
                    // If nobody is detected, show tasks for both mom and mom + child
                    if (getBodyCount() == 0)
                    {
                        if (task.owners.Contains("Lisa") && !task.owners.Contains("Tim") && task.dueDate.Month == selectedPeriod.Month) 
                        {
                            scrollContentLisa.Children.Add(button);
                        }
                        else if (task.owners.Contains("Lisa") && task.owners.Contains("Tim") && task.dueDate.Month == selectedPeriod.Month) 
                        {
                            scrollContentTim.Children.Add(button);
                        }

                        
                        timer.Start();    
                    }
                    // If only mom is present, show tasks for mom
                    else if (getBodyCount() == 1)
                    {
                        if (task.owners.Contains("Lisa") && !task.owners.Contains("Tim") && task.dueDate.Month == selectedPeriod.Month)
                        {
                            scrollContent.Children.Add(button);
                        }
                        
                    }

                    // If both mom and child are present, show all tasks
                    else if (getBodyCount() == 2)
                    {
                        if (task.owners.Contains("Lisa") && task.owners.Contains("Tim") && task.dueDate.Month == selectedPeriod.Month)
                        {
                            scrollContent.Children.Add(button);
                        }
                        
                    }
                }
        }



        void timer_Tick(object sender, EventArgs e)
        {
            scrollViewerDouble1.ScrollToHorizontalOffset(scrollAmount);
            scrollViewerDouble2.ScrollToHorizontalOffset(scrollAmount);
            if (scrollForward && scrollAmount < scrollViewerDouble1.ScrollableWidth)
            {
                scrollAmount++;
                if (scrollAmount == scrollViewerDouble1.ScrollableWidth || scrollAmount == scrollViewerDouble2.ScrollableWidth)
                {
                    scrollForward = false;
                }
            }
            else if (!scrollForward && scrollAmount > 0)
            {               
                scrollAmount--;
                if (scrollAmount == 0 || scrollAmount == 0)
                {
                    scrollForward = true;
                }
            }
        }

        private void addButton_Click(object sender, RoutedEventArgs e)
        {

            List<string> owners = new List<string>();
            Debug.WriteLine("Add task body count: " + getBodyCount());
            if (getBodyCount() == 1)
            {
                owners.Add("Lisa");
                
            }
            else if (getBodyCount() == 2)
            {
                owners.Add("Lisa");
                owners.Add("Tim");
                
            }


            tasks.Add(new Task(owners, taskNameBox.Text, detailsBox.Text, dueDate.SelectedDate ?? DateTime.Now, new string((Regex.Replace(taskNameBox.Text, @"\s+", "")).Where(c => !char.IsPunctuation(c)).ToArray())));

            addTaskGrid.Visibility = Visibility.Collapsed;
            currentPeriod.Visibility = Visibility.Visible;
            singleTaskLabel.Visibility = Visibility.Visible;
            lisaDoubleTaskLabel.Visibility = Visibility.Visible;
            timDoubleTaskLabel.Visibility = Visibility.Visible;
            kinectGrid.Visibility = Visibility.Visible;
            

                var button = new KinectTileButton
                {
                    Name = new string((Regex.Replace(taskNameBox.Text, @"\s+", "")).Where(c => !char.IsPunctuation(c)).ToArray()),
                    FontSize = 20,
                    Content = taskNameBox.Text + "\n\n" + "Due: " + dueDate.SelectedDate.Value.ToShortDateString() + "\n\nDetails: " + detailsBox.Text,
                    Height = 100,
                    HorizontalLabelAlignment = HorizontalAlignment.Left,
                    Foreground = Brushes.Black,
                    Background = Brushes.Beige
                };
                button.Click += removeTask_Click;
                DateTime temp = dueDate.SelectedDate ?? DateTime.Now;
                if (temp.Month == selectedPeriod.Month)
                {
                        scrollContent.Children.Add(button);       
                }

        }

        void removeTask_Click(object sender, RoutedEventArgs e)
        {
            KinectTileButton temp = (KinectTileButton)sender;

            for (int i = 0; i < tasks.Count; i++)
            {
                Debug.WriteLine(temp.Name);
                if (temp.Name == tasks[i].taskID)
                {
                    tasks.Remove(tasks[i]);
                }
            }
            scrollContent.Children.Remove(temp);
        }

        private void addTask_Click(object sender, RoutedEventArgs e)
        {
            
            addTaskGrid.Visibility = Visibility.Visible;
            currentPeriod.Visibility = Visibility.Collapsed;
            singleTaskLabel.Visibility = Visibility.Collapsed;
            lisaDoubleTaskLabel.Visibility = Visibility.Collapsed;
            timDoubleTaskLabel.Visibility = Visibility.Collapsed;
            kinectGrid.Visibility = Visibility.Collapsed;
            if (getBodyCount() == 1)
                addTaskLabel.Text = "Add task for Lisa";
            else if (getBodyCount() == 2)
                addTaskLabel.Text = "Add task for Lisa and Tim";
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            addTaskGrid.Visibility = Visibility.Collapsed;
            currentPeriod.Visibility = Visibility.Visible;
            singleTaskLabel.Visibility = Visibility.Visible;
            lisaDoubleTaskLabel.Visibility = Visibility.Visible;
            timDoubleTaskLabel.Visibility = Visibility.Visible;
            kinectGrid.Visibility = Visibility.Visible;
            addTaskButton.Visibility = Visibility.Visible;
            userViewer.Visibility = Visibility.Visible;
            refreshTasks();
            
        }


    }
}

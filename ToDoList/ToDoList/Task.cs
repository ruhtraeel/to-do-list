﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToDoList
{
    class Task
    {
        public List<string> owners = new List<string>();
        public string taskName = "";
        public string details = "";
        public DateTime dueDate;
        public string taskID;
        public Task(List<string> owners, string taskName, string details, DateTime dueDate, string taskID)
        {
            this.owners = owners;
            this.taskName = taskName;
            this.details = details;
            this.dueDate = dueDate;
            this.taskID = taskID;
        }
    }
}
